package com.novina.mobile.application

import android.support.annotation.UiThread
import android.support.multidex.MultiDexApplication
import com.novina.mobile.BuildConfig
import com.novina.mobile.di.components.ApplicationComponent
import com.novina.mobile.di.components.DaggerApplicationComponent
import com.novina.mobile.di.modules.ApplicationModule
import timber.log.Timber

class MainApplication : MultiDexApplication() {

    private lateinit var mApplicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        setupTimber()
        getApplicationComponent().inject(this)

    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    @UiThread
    fun getApplicationComponent() : ApplicationComponent {
        if (!::mApplicationComponent.isInitialized){
            mApplicationComponent = DaggerApplicationComponent
                    .builder()
                    .applicationModule(ApplicationModule(this))
                    .build()
        }
        return mApplicationComponent
    }

}
