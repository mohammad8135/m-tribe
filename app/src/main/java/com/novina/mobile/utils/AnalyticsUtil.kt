package com.novina.mobile.utils

class AnalyticsUtil {
    companion object {
        const val AD_EVENT = "ad_event"
        const val RESULT_PARAM = "result"
        const val AD_TYPE_PARAM = "ad_type"
        const val ERROR_PARAM = "error_message"
        const val ERROR_EVENT = "error_event"
        const val MAP_ADD_MARKER_EVENT = "maps_markers"
        const val MAP_ADD_MARKER_PARAM = "number_of_markers"
        const val MAP_MARKER_CLICK_EVENT = "map_marker_click"
    }
}