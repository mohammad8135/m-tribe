package com.novina.mobile.utils

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.novina.mobile.R
import com.novina.mobile.di.modules.GlideApp

class ImageLoader() {

    fun loadImageWithProgressbar(imageView: ImageView, url: String) {
        val options = RequestOptions()
        GlideApp.with(imageView.context).load(url)
                .apply(options.fitCenter().placeholder(R.drawable.bg_progress_bar))
                .into(imageView)
    }

    fun loadImage(imageView: ImageView, url: String) {
        val options = RequestOptions()
        GlideApp.with(imageView.context).load(url)
                .apply(options.centerCrop())
                .into(imageView)
    }


}