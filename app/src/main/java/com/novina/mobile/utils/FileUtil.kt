package com.novina.mobile.utils

import android.content.Context
import java.io.File
import java.io.IOException
import java.nio.charset.StandardCharsets

object FileUtil {

    @JvmStatic
    fun loadFileFromAsset(context: Context, fileName: String): String? {
        var content: String? = null
        try {
            val inputStream = context.assets.open(fileName)
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.read(buffer)
            inputStream.close()
            content = String(buffer, StandardCharsets.UTF_8)
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
        return content
    }

    @JvmStatic
    fun loadFileFromCache(context: Context, fileName: String): String? {
        var content: String? = null
        try {
            val file = File(context.cacheDir, fileName)
            when (file.exists() && file.isFile) {
                true -> {
                    val inputStream = file.inputStream()
                    val size = inputStream.available()
                    val buffer = ByteArray(size)
                    inputStream.read(buffer)
                    inputStream.close()
                    content = String(buffer, StandardCharsets.UTF_8)
                }
            }
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
        return content
    }

    @JvmStatic
    fun isCachedFileAvailable(context: Context, fileName: String): Boolean {
        try {
            val file = File(context.cacheDir, fileName)
            return (file.exists() && file.isFile)
        } catch (ex: IOException) {
            ex.printStackTrace()
        }
        return false
    }
}