package com.novina.mobile.views.map

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.v13.app.ActivityCompat
import android.support.v13.app.ActivityCompat.requestPermissions
import android.support.v4.content.ContextCompat
import android.widget.Toast
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.firebase.analytics.FirebaseAnalytics
import com.novina.mobile.R
import com.novina.mobile.repository.model.ItemDetail
import com.novina.mobile.utils.AnalyticsUtil
import com.novina.mobile.views.BaseActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_maps.*
import timber.log.Timber
import javax.inject.Inject

class MapsActivity : BaseActivity(), OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMarkerClickListener {

    @Inject
    lateinit var viewModel: MapsViewModel
    private val itemList: ArrayList<ItemDetail> = ArrayList()
    private var selectedItem: ItemDetail? = null
    private var map: GoogleMap? = null
    private val markerInfoList = HashMap<Marker, ItemDetail>()
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var userLocation: LatLng? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        getControllerComponent().inject(this)
        super.onCreate(savedInstanceState)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        setupToolbar()
        setupAnalytics()
        mapFragment.getMapAsync(this)
        getIntentData()
        loadData()
        getUseLocation()
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_maps
    }

    private fun getIntentData() {
        if (intent.hasExtra(KEY_SELECTED_ITEM_EXTRA))
            selectedItem = intent.getParcelableExtra(KEY_SELECTED_ITEM_EXTRA)
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun loadData() {
        disposables.add(viewModel.getPlaceMarkItems()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result -> populateView(result) },
                        { error -> showError(error?.message) }
                ))
    }

    private fun populateView(result: List<ItemDetail>) {
        this.itemList.clear()
        this.itemList.addAll(result)
        map?.let { populateMarkers() }
    }

    /*
     * Populate the selectedItem if exist, otherwise populate all the items
     * If user's location exist, add it with different icon
     * Animate the map to cover all the markers
     */
    private fun populateMarkers() {
        map?.clear()
        markerInfoList.clear()
        // If user select any marker -> Only show that marker with opened info window
        selectedItem.let{
            if (it != null){
            val latLng = LatLng(selectedItem!!.coordinates[1], selectedItem!!.coordinates[0])
            val marker = map?.addMarker(MarkerOptions().position(latLng).title(selectedItem!!.name))
            map?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18f))
            marker?.showInfoWindow()
            trackMarkerClickEvent(marker)
        // Else populate the map with all the markers including the user latest location
        } else {
            var latLng: LatLng?
            val latLngBoundBuilder = LatLngBounds.Builder()
                if (itemList.isNotEmpty()) {
                    for (item in itemList) {
                        latLng = LatLng(item.coordinates[1], item.coordinates[0])
                        val marker = map?.addMarker(MarkerOptions().position(latLng).title(item.name))
                        marker?.let {
                            markerInfoList.put(marker, item)
                            latLngBoundBuilder.include(marker.position)
                        }
                    }
                    userLocation?.let {
                        val marker = map?.addMarker(MarkerOptions().position(it).title("Me").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_user_location)))
                        marker?.let { latLngBoundBuilder.include(it.position) }
                    }
                    val bounds: LatLngBounds = latLngBoundBuilder.build()
                    val width = resources.displayMetrics.widthPixels
                    val height = resources.displayMetrics.heightPixels
                    val padding = (width * 0.10).toInt() // offset from edges of the map 10% of screen
                    val cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds,width, height, padding)
                    map?.animateCamera(cameraUpdate)
                    trackPopulateMarkersEvent()
                }
        }}
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        marker?.let {
            if (marker.title != "Me") {
                selectedItem = markerInfoList[marker]
                populateMarkers()
            }
        }
        return true
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map?.setOnInfoWindowClickListener(this)
        map?.setOnMarkerClickListener(this)
        populateMarkers()
    }

    private fun showError(message: String?) {
        Timber.e(message)
        trackErrorEvent(message)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onInfoWindowClick(marker: Marker?) {
        marker?.let {
            Toast.makeText(this, marker.title, Toast.LENGTH_SHORT).show()
            trackMarkerClickEvent(marker)
        }
    }

    fun setupAnalytics(){
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
    }

    fun trackPopulateMarkersEvent(){
        val bundle = Bundle()
        bundle.putString(AnalyticsUtil.MAP_ADD_MARKER_PARAM, markerInfoList.size.toString())
        firebaseAnalytics.logEvent(AnalyticsUtil.MAP_MARKER_CLICK_EVENT, bundle)
    }

    fun trackMarkerClickEvent(marker: Marker?){
        marker?.let { val bundle = Bundle()
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, marker.title)
            bundle.putString(FirebaseAnalytics.Param.ITEM_LOCATION_ID, marker.position.toString())
            firebaseAnalytics.logEvent(AnalyticsUtil.MAP_MARKER_CLICK_EVENT, bundle)
        }
    }

    private fun trackErrorEvent(error: String?) {
        val bundle = Bundle()
        bundle.putString(AnalyticsUtil.ERROR_PARAM, error)
        firebaseAnalytics.logEvent(AnalyticsUtil.ERROR_EVENT, bundle)
    }

    private fun getUseLocation(){
        checkLocationPermission()
    }

    private fun checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {
                val runnable = Runnable { showPermissionRequestRationale() }
                runnable.run()
            } else {
                // No explanation needed, we can request the permission.
                // Best to show why we need the permission, so lets ask user first
                showPermissionRequestRationale()
            }
        } else {
            // Permission granted, find user location
            findUserFineLocation()
        }
    }

    private fun requestLocationPermission(){
        requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
    }

    private fun showPermissionRequestRationale(){
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.title_ask_location_permission_dialog))
        builder.setMessage(getString(R.string.message_ask_location_permission_dialog))
        builder.setPositiveButton(getString(R.string.cta_proceed)){ dialog, which ->
            dialog.dismiss()
            requestLocationPermission()
        }
        builder.setNegativeButton(getString(R.string.cta_no)){ dialog, which ->
            dialog.dismiss()
            // Do nothing
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    findUserFineLocation()
                } else {
                    // The permission is not granted, do nothing
                }
                return
            }
            else -> {
                // Ignore all other requests.
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun findUserFineLocation(){
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    location?.let {
                        userLocation = LatLng(location.latitude, location.longitude)
                        populateMarkers() }
                }
    }


    companion object {
        const val KEY_SELECTED_ITEM_EXTRA = "KEY_SELECTED_ITEM_EXTRA"
        const val MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 102
        @JvmStatic
        fun startMapsActivity(context: Context, selectedItem: ItemDetail?) {
            val intent = Intent(context, MapsActivity::class.java)
            selectedItem?.let{ if (selectedItem != null) intent.putExtra(KEY_SELECTED_ITEM_EXTRA, selectedItem) }
            context.startActivity(intent)
        }
    }
}
