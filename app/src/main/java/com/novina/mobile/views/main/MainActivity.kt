package com.novina.mobile.views.main

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.google.firebase.analytics.FirebaseAnalytics
import com.novina.mobile.BuildConfig
import com.novina.mobile.R
import com.novina.mobile.repository.model.ItemDetail
import com.novina.mobile.utils.AnalyticsUtil
import com.novina.mobile.utils.ImageLoader
import com.novina.mobile.views.BaseActivity
import com.novina.mobile.views.ItemRecyclerViewAdapter
import com.novina.mobile.views.map.MapsActivity
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import javax.inject.Inject
import com.crashlytics.android.Crashlytics
import com.novina.mobile.utils.NetworkUtil
import io.fabric.sdk.android.Fabric
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class MainActivity : BaseActivity() {

    lateinit var adapter: ItemRecyclerViewAdapter
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private var startEngagementTime = 0L

    @Inject
    lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        getControllerComponent().inject(this)
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        startEngagementTime = System.currentTimeMillis()
        setupToolbar()
        setupRecyclerView()
        setupAnalytics()
        bind()
    }

    /*
     * Bind the view with repository to load data
     */
    private fun bind(){
        disposables.add(viewModel.getPlaceMarkItems()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { result -> populateData(result) },
                        { error -> showError(error?.message) })
        )
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        ImageLoader().loadImage(heroImage, BuildConfig.APP_HERO_IMAGE_URL)
    }

    private fun setupRecyclerView() {
        rvList.layoutManager = LinearLayoutManager(rvList.context)
        rvList.addItemDecoration(DividerItemDecoration(rvList.context, DividerItemDecoration.VERTICAL))

        adapter = ItemRecyclerViewAdapter()
        rvList.adapter = adapter
        adapter.getItemClickRelay().subscribe(this::onItemDetailSelected)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    private fun showError(error: String?) {
        pgBar.visibility = View.GONE
        txtMessage.visibility = View.VISIBLE
        Timber.e(error)
        trackErrorEvent(error)
    }

    private fun populateData(result: List<ItemDetail>) {
        pgBar.visibility = View.GONE
        when(result.size){
            0 -> {
                txtMessage.visibility = View.VISIBLE
                if (NetworkUtil.isNetworkAvailable(this)) txtMessage.text = getString(R.string.txt_no_internet)
                else this.txtMessage.text = getString(R.string.empty_list_message)
            }
            else -> txtMessage.visibility = View.GONE
        }
        adapter.setItemList(result)
    }

    private fun onItemDetailSelected(itemDetail: ItemDetail) {
        trackItemSelectionEvent(itemDetail)
        Toast.makeText(this, itemDetail.vin, Toast.LENGTH_SHORT).show()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater?.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId == R.id.map) {
            true -> {
                MapsActivity.startMapsActivity(this, null)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupAnalytics(){
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)
    }

    private fun trackItemSelectionEvent(itemDetail: ItemDetail){
        val bundle = Bundle()
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, itemDetail.vin)
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, itemDetail.name)
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle)
    }

    private fun trackErrorEvent(error: String?) {
        val bundle = Bundle()
        bundle.putString(AnalyticsUtil.ERROR_PARAM, error)
        firebaseAnalytics.logEvent(AnalyticsUtil.ERROR_EVENT, bundle)
    }
}
