package com.novina.mobile.views

import android.support.annotation.UiThread
import android.support.v4.app.Fragment
import com.novina.mobile.application.MainApplication
import com.novina.mobile.di.components.ControllerComponenets
import com.novina.mobile.di.modules.ControllerModule
import io.reactivex.disposables.CompositeDisposable

open class BaseFragment : Fragment() {

    protected val disposables = CompositeDisposable()

    @UiThread
    protected fun getControllerComponent(): ControllerComponenets {
        return (activity?.application as MainApplication).getApplicationComponent().injectControllerComponent(ControllerModule(activity))
    }

    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }

}
