package com.novina.mobile.views

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.novina.mobile.R
import com.novina.mobile.repository.model.ItemDetail
import io.reactivex.subjects.PublishSubject

class ItemRecyclerViewAdapter : RecyclerView.Adapter<ItemViewHolder>() {

    private var itemList: ArrayList<ItemDetail> = ArrayList()
    private val publishRelay = PublishSubject.create<ItemDetail>()

    fun setItemList(itemList: List<ItemDetail>){
        this.itemList.clear()
        this.itemList.addAll(itemList)
        this.notifyDataSetChanged()
    }

    fun getItemClickRelay(): PublishSubject<ItemDetail>{
        return publishRelay
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_main, parent, false)
        return ItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun onBindViewHolder(viewHolder: ItemViewHolder, position: Int) {
        viewHolder.tvName.text = itemList.get(position).name
        viewHolder.tvVin.text = itemList.get(position).vin
        viewHolder.tvEngineType.text = itemList.get(position).engineType
        viewHolder.tvExterior.text = itemList.get(position).exterior
        viewHolder.tvInterior.text = itemList.get(position).interior
        viewHolder.tvAddress.text = itemList.get(position).address
        viewHolder.fuelGuage.value = itemList.get(position).fuel
        viewHolder.itemView.setOnClickListener {
            publishRelay.onNext(itemList.get(position))
        }
    }
}


