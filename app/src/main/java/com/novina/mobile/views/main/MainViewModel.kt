package com.novina.mobile.views.main

import com.novina.mobile.repository.IItemRepository
import com.novina.mobile.repository.model.ItemDetail
import com.novina.mobile.views.BaseViewModel
import io.reactivex.Observable

open class MainViewModel(itemRepository: IItemRepository) : BaseViewModel() {

    private  val itemRepository: IItemRepository = itemRepository

    open fun getPlaceMarkItems(): Observable<List<ItemDetail>> {
        return itemRepository.getItemDetailList()
    }
}