package com.novina.mobile.views

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.list_item_main.view.*


class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val avatar = view.avatar
    val tvName = view.tvName
    val tvVin = view.tvVin
    val tvInterior = view.tvInterior
    val tvExterior = view.tvExterior
    val tvEngineType = view.tvEngineType
    val tvAddress = view.tvAddress
    val fuelGuage = view.ggFuel

}