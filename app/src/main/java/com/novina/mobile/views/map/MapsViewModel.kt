package com.novina.mobile.views.map

import com.novina.mobile.repository.IItemRepository
import com.novina.mobile.repository.model.ItemDetail
import com.novina.mobile.views.BaseViewModel
import io.reactivex.Observable

open class MapsViewModel(itemRepository: IItemRepository) : BaseViewModel() {

    private val itemRepository: IItemRepository = itemRepository

    open fun getPlaceMarkItems(): Observable<List<ItemDetail>> {
        return itemRepository.getItemDetailList()
    }
}
