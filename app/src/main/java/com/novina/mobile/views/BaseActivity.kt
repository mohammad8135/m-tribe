package com.novina.mobile.views

import android.os.Bundle
import android.support.annotation.UiThread
import android.support.v7.app.AppCompatActivity
import com.novina.mobile.application.MainApplication
import com.novina.mobile.di.components.ControllerComponenets
import com.novina.mobile.di.modules.ControllerModule
import io.reactivex.disposables.CompositeDisposable


open abstract class BaseActivity : AppCompatActivity() {

    lateinit var disposables: CompositeDisposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        disposables = CompositeDisposable()
        setContentView(getLayoutId())
    }

    @UiThread
    protected fun getControllerComponent(): ControllerComponenets {
        return (application as MainApplication).getApplicationComponent().injectControllerComponent(ControllerModule(this))
    }

    override fun onDestroy() {
        disposables?.dispose()
        super.onDestroy()
    }

    abstract fun getLayoutId(): Int
}