package com.novina.mobile.repository.remote

import com.novina.mobile.repository.model.ItemDetail
import com.novina.mobile.repository.model.PlaceMark
import io.reactivex.Observable

class RemoteService(placeMarkApi: PlaceMarkApiService) {

    private val placeMarkApiService: PlaceMarkApiService = placeMarkApi

    fun getItemDetailList(): Observable<List<ItemDetail>> {
        return placeMarkApiService.getPlaceMark()
                .map { placeMarks: PlaceMark -> placeMarks.list }
    }
}