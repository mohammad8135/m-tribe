package com.novina.mobile.repository.persistence

import com.novina.mobile.repository.model.ItemDetail
import io.reactivex.Flowable

/**
 * CacheService is responsible to persist data
 *
 */
class CacheService(itemDetailDao: ItemDetailDao) {

    private val itemDetailDao = itemDetailDao

    fun getItemDetailList(): Flowable<List<ItemDetail>> {
        return itemDetailDao.getAll()
    }

    fun persistItemDetals(items: List<ItemDetail>):List<Long> {
        return itemDetailDao.insertAll(items)
    }

    fun deleteAllItemDetails(){
        itemDetailDao.deleteAll()
    }
}