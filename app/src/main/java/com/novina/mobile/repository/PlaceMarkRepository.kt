package com.novina.mobile.repository

import com.novina.mobile.repository.model.ItemDetail
import com.novina.mobile.repository.persistence.CacheService
import com.novina.mobile.repository.remote.RemoteService
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class PlaceMarkRepository(private val cacheService: CacheService,
                          private val remoteService: RemoteService) : IItemRepository {

    /*
     * Retrieve data from DB
     * If DB is empty -> Initiate calling API -> Store data in DB
     * Return obtained data to observer
     */
    override fun getItemDetailList(): Observable<List<ItemDetail>> {
        return getDataFromDb()
                .flatMap {
                    if (it.isNotEmpty()){
                        Timber.d("Dispatch item details from DB, ${it.size} ")
                         Observable.just(it)
                    } else {
                        Timber.d("DB is empty -> initiate getData from API ... ")
                        getDataFromApi()
                    }
                }
    }


    private fun getDataFromDb(): Observable<List<ItemDetail>> {
        return cacheService.getItemDetailList().toObservable()
    }

    private fun getDataFromApi(): Observable<List<ItemDetail>> {
        return remoteService.getItemDetailList()
                .doOnNext {
                    Timber.d("Dispatching ${it.size} Item details from API...")
                    storeDataInDb(it)
                }.doOnError { Timber.e("Dispatching error: ${it.message} Item details from API...") }
    }

    private fun storeDataInDb(items: List<ItemDetail>) {
        Single.fromCallable { cacheService.persistItemDetals(items) }
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(
                        { Timber.d("Inserted ${items.size} data from API in DB...")},
                        { error -> Timber.d("Insert data from API in DB failed, " + error.message) })
    }

}