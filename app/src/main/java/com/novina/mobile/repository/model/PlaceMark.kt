package com.novina.mobile.repository.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlaceMark(@SerializedName("placemarks") val list: List<ItemDetail> = ArrayList()) : Parcelable