package com.novina.mobile.repository.remote

import com.novina.mobile.repository.model.PlaceMark
import io.reactivex.Observable
import retrofit2.http.GET

interface PlaceMarkApiService {
    @GET("/locations")
    fun getPlaceMark(): Observable<PlaceMark>
}