package com.novina.mobile.repository.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "itemdetails")
@Parcelize
data class ItemDetail(
        @PrimaryKey
        @SerializedName("vin")
        val vin: String,
        @SerializedName("name")
        val name: String,
        @SerializedName("interior")
        val interior: String,
        @SerializedName("coordinates")
        val coordinates: DoubleArray,
        @SerializedName("fuel")
        val fuel: Int,
        @SerializedName("address")
        val address: String,
        @SerializedName("exterior")
        val exterior: String,
        @SerializedName("engineType")
        val engineType: String) : Parcelable {


    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ItemDetail

        if (name != other.name) return false
        if (interior != other.interior) return false
        if (!coordinates.contentEquals(other.coordinates)) return false
        if (fuel != other.fuel) return false
        if (vin != other.vin) return false
        if (address != other.address) return false
        if (exterior != other.exterior) return false
        if (engineType != other.engineType) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + interior.hashCode()
        result = 31 * result + coordinates.contentHashCode()
        result = 31 * result + fuel
        result = 31 * result + vin.hashCode()
        result = 31 * result + address.hashCode()
        result = 31 * result + exterior.hashCode()
        result = 31 * result + engineType.hashCode()
        return result
    }
}