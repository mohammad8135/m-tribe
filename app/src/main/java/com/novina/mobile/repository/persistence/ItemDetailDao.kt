package com.novina.mobile.repository.persistence

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.novina.mobile.repository.model.ItemDetail
import io.reactivex.Flowable

@Dao
interface ItemDetailDao {
    @Query("SELECT * FROM itemdetails")
    fun getAll(): Flowable<List<ItemDetail>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(items: List<ItemDetail>): List<Long>

    @Query("DELETE FROM itemdetails")
    fun deleteAll()
}