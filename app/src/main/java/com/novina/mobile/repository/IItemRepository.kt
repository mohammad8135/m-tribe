package com.novina.mobile.repository

import com.novina.mobile.repository.model.ItemDetail
import io.reactivex.Observable

interface IItemRepository {
    fun getItemDetailList() : Observable<List<ItemDetail>>
}