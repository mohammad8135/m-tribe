package com.novina.mobile.repository.persistence

import android.arch.persistence.room.TypeConverter
import android.text.TextUtils
import com.google.gson.Gson
import com.google.gson.JsonArray

/*
 * Help to store array of double in room data persistent
 * by convert DoubleArray to String and reverse
 *
 */
class CustomTypeConvertors {

    @TypeConverter
    fun stringToDoubleArray(value: String?): DoubleArray? {
        var doubleArray: DoubleArray? = null
        value?.let {
            if (!TextUtils.isEmpty(value)){
                doubleArray = Gson().fromJson(value, DoubleArray::class.java)
            }
        }
        return doubleArray
    }

    @TypeConverter
    fun doubleArrayToString(array: DoubleArray?): String? {
        val jsonArray = JsonArray()
        val numbersIterator = array?.iterator()
        while (numbersIterator!!.hasNext()) {
            jsonArray.add(numbersIterator.nextDouble())
        }
        return jsonArray.toString()
    }
}