package com.novina.mobile.repository.persistence

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.novina.mobile.repository.model.ItemDetail
import android.arch.persistence.room.Room
import android.arch.persistence.room.TypeConverters


@Database(entities = arrayOf(ItemDetail::class), version = 1)
@TypeConverters(CustomTypeConvertors::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun itemDetailDao(): ItemDetailDao

    companion object {
        @Volatile private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context)= instance ?: synchronized(LOCK){
            instance ?: buildDatabase(context).also { instance = it}
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(context,
                AppDatabase::class.java, "placemarks.db")
                .build()
    }
}