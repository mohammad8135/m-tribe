package com.novina.mobile.di.modules

import android.app.Activity
import com.novina.mobile.di.scopes.PerActivity

import dagger.Module
import dagger.Provides

@Module
class ControllerModule(context : Activity) {

    private var context : Activity = context

    @Suppress("DEPRECATION")
    @PerActivity
    @Provides
    fun fragmentManager() : android.app.FragmentManager? {
        return context.fragmentManager
    }
}