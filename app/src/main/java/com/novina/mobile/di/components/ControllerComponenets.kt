package com.novina.mobile.di.components

import com.novina.mobile.di.modules.ControllerModule
import com.novina.mobile.views.main.MainActivity
import com.novina.mobile.views.map.MapsActivity
import dagger.Subcomponent

@Subcomponent(modules = [ControllerModule::class])
interface ControllerComponenets {

    fun inject(mainActivity: MainActivity)

    fun inject(mapsActivity: MapsActivity)
}