package com.novina.mobile.di.modules

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import com.google.gson.Gson
import okhttp3.OkHttpClient
import com.google.gson.GsonBuilder
import com.novina.mobile.BuildConfig
import com.novina.mobile.di.scopes.PerApplication
import com.novina.mobile.repository.remote.PlaceMarkApiService


@Module (includes = [NetworkModule::class])
class RemoteServiceModule {

    @PerApplication
    @Provides
    fun locationService(retrofit: Retrofit) : PlaceMarkApiService = retrofit.create(PlaceMarkApiService::class.java)

    @PerApplication
    @Provides
    fun gson(): Gson {
        val gsonBuilder = GsonBuilder()
        return gsonBuilder.create()
    }

    @PerApplication
    @Provides
    fun retrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .baseUrl(BuildConfig.BASE_URL)
                .build()
    }

}