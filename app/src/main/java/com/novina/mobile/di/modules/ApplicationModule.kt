package com.novina.mobile.di.modules

import android.app.Application
import android.content.Context
import com.novina.mobile.di.scopes.PerApplication
import dagger.Module
import dagger.Provides

/**
 * Module used to provide dependencies at an application-level.
 */
@Module
open class ApplicationModule(application : Application) {

    private var application: Application = application

    @Provides
    @PerApplication
    fun application(): Application {
        return application
    }

    @Provides
    @PerApplication
    fun context(): Context {
        return application.applicationContext
    }

}
