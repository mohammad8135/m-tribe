package com.novina.mobile.di.modules

import android.content.Context
import com.novina.mobile.repository.IItemRepository
import com.novina.mobile.repository.PlaceMarkRepository
import com.novina.mobile.repository.persistence.AppDatabase
import com.novina.mobile.repository.persistence.CacheService
import com.novina.mobile.repository.remote.PlaceMarkApiService
import com.novina.mobile.repository.remote.RemoteService
import com.novina.mobile.views.main.MainViewModel
import com.novina.mobile.views.map.MapsViewModel
import dagger.Module
import dagger.Provides

@Module
class ViewModelModule {

    @Provides
    fun mainViewModel(itemRepository: IItemRepository): MainViewModel {
        return MainViewModel(itemRepository)
    }

    @Provides
    fun itemRepository(cacheService: CacheService,
                             remoteService: RemoteService): IItemRepository {
        return PlaceMarkRepository(cacheService, remoteService)
    }

    @Provides
    fun cacheService(context: Context): CacheService {
        val database = AppDatabase.invoke(context)
        return CacheService(database.itemDetailDao())
    }

    @Provides
    fun remoteService(locationApi: PlaceMarkApiService): RemoteService {
        return RemoteService(locationApi)
    }

    @Provides
    fun mapsViewModel(itemRepository: IItemRepository): MapsViewModel {
        return MapsViewModel(itemRepository)
    }
}