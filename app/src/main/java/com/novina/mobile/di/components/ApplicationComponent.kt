package com.novina.mobile.di.components

import com.novina.mobile.application.MainApplication
import com.novina.mobile.di.modules.*
import com.novina.mobile.di.scopes.PerApplication
import dagger.Component

@PerApplication
@Component(modules = [ApplicationModule::class, RemoteServiceModule::class, ViewModelModule::class])
interface ApplicationComponent {

    fun inject(application: MainApplication)

    fun injectControllerComponent(controllerModule: ControllerModule) : ControllerComponenets

}
